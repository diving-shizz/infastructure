variable "name" {
  default = "diving-shizz"
}

variable "organization" {
  default = "diving-shizz"
}

variable "personal" {
  default = false
}

variable "region" {
  default = "eu"
}

variable "db_plan" {
  default = "heroku-postgresql:hobby-dev"
}
