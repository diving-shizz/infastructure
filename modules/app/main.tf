resource "heroku_app" "this" {
  name   = var.name
  region = var.region

  organization {
    name     = var.organization
    personal = var.personal
  }

  buildpacks = [
    "https://github.com/moneymeets/python-poetry-buildpack.git",
    "heroku/python"
  ]
}

resource "heroku_addon" "this" {
  app  = heroku_app.this.name
  plan = var.db_plan
}
