terraform {
  backend "gcs" {
    bucket = "divingshizz-terraform-state"
    prefix = "terraform/live/state"
  }

  required_providers {
    heroku = {
      source  = "heroku/heroku"
      version = "4.6.0"
    }
  }
}

provider "heroku" {}

module "live_app" {
  source = "../modules/app"

  name     = "live-diving-shizz"
  personal = true
}
