terraform {
  backend "gcs" {
    bucket = "divingshizz-terraform-state"
    prefix = "terraform/burnside/state"
  }

  required_providers {
    heroku = {
      source  = "heroku/heroku"
      version = "4.6.0"
    }
  }
}

provider "heroku" {}

module "live_app" {
  source = "../modules/app"

  name     = "burnside-diving-shizz"
  personal = true
}
